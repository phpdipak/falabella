<?php
$divisible_by_3 = array("","Linio");
$divisible_by_5 = array("","IT");

for($i=1; $i<=100; $i++){

    if( ($i%3 == 0) && ($i%5==0) ){
        echo "Linianos";
        continue;
    }
    
    echo $divisible_by_3[( ($i%3 <=> 0) == 0)];
    echo $divisible_by_5[( ($i%5 <=> 0) == 0)];
    echo "\n";
}
